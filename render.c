#include <stdio.h>
#include<SDL2/SDL.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL2_gfxPrimitives.h>
#include "game.h"
extern int b[N][N];
extern int k[N][N];
extern TTF_Font* font; 
SDL_Surface* surfaceMessage = NULL;
SDL_Texture* message = NULL;
SDL_Rect Message_rect;
SDL_Color black = {0x00, 0x00, 0x00, 0xFF};
SDL_Color red = {0xFF, 0x00, 0x00, 0xFF};
SDL_Color *color;
char a[2]={'0', '\0'};
void render_game(SDL_Renderer *renderer){
	int thickness=1;
	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(renderer);
	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
	TTF_SetFontHinting(font, TTF_HINTING_MONO);
	for(int i=0; i <= N; ++i){
		if(i%3==0)
			thickness=4;
		else
			thickness=1;
		thickLineRGBA(renderer, BORDER+(i*CELL_WIDTH), 0, BORDER+(i*CELL_WIDTH), SCREEN_HEIGHT, thickness, 0x00, 0x00, 0x00, 0xFF);
		thickLineRGBA(renderer, BORDER , i*CELL_HEIGHT, SCREEN_WIDTH-BORDER, i*CELL_HEIGHT, thickness, 0x00, 0x00, 0x00, 0xFF);
	}
	for(int y=0; y < N; ++y){
		for(int x=0; x < N; ++x){
			if(!b[x][y])
				continue;
			a[0]=b[x][y]+'0';
			if(k[x][y])
				color=&red;
			else
				color=&black;
			surfaceMessage = TTF_RenderText_Blended(font, a, *color);
			message = SDL_CreateTextureFromSurface(renderer, surfaceMessage);
			Message_rect.x=BORDER+(CELL_WIDTH*x)+CELL_WIDTH*((1-NUMBER_SCALE_X)/2)+CELL_WIDTH*((1-NUMBER_SCALE)/2);
			Message_rect.y=y*CELL_HEIGHT+CELL_HEIGHT*((1-NUMBER_SCALE)/2);
			Message_rect.w=CELL_WIDTH*NUMBER_SCALE*NUMBER_SCALE_X;
			Message_rect.h=CELL_HEIGHT*NUMBER_SCALE;
			SDL_RenderCopy(renderer, message, NULL, &Message_rect);
			SDL_DestroyTexture(message);
			message=NULL;
		}
	}
	SDL_RenderPresent(renderer);
}
void click_on_cell(SDL_Renderer *renderer, int x, int y){
	render_game(renderer);
	SDL_SetRenderDrawColor(renderer, 0xE6, 0xF7, 0x00, 0xFF);
	SDL_Rect fillRect = { BORDER+(CELL_WIDTH*x)+1, y*CELL_HEIGHT+1, CELL_WIDTH-1, CELL_HEIGHT-1};
	SDL_RenderFillRect( renderer, &fillRect );
	SDL_RenderPresent(renderer);
}
