#pragma once
#include "game.h"
int find_solution();
int* find_unassigned();
int check_row(int x, int y, int a);
int check_column(int x, int y, int a);
int check_square(int x, int y, int a);
int check(int x, int y, int a);
typedef int (*check_function) (int, int, int);
