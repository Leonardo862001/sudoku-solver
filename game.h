#pragma once
#define SCREEN_WIDTH 720
#define SCREEN_HEIGHT 720
#define N 9
#define CELL_WIDTH (int) (SCREEN_HEIGHT/9)
#define CELL_HEIGHT CELL_WIDTH
#define BORDER (int) ((SCREEN_WIDTH-(CELL_WIDTH*N))/2)
#define FONT_SIZE 75
#define NUMBER_SCALE_X 0.6
#define NUMBER_SCALE 0.75
