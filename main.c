#include<stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "game.h"
#include "render.h"
#include "logic.h"
int b[N][N];
int k[N][N];
TTF_Font *font;
int w=0;
int main()
{
	int quit=0;
	int cell[2]={0, 0};
	for(int x=0; x < N; ++x){
		for(int y=0; y< N; ++y){
			b[x][y]=0;
			k[x][y]=0;
		}
	}
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        fprintf(stderr, "Unable to initialize SDL: %s", SDL_GetError());
        return 1;
	}
        SDL_Window *window = SDL_CreateWindow("Procedural",
                                          100, 100,
                                          SCREEN_WIDTH, SCREEN_HEIGHT,
					  SDL_WINDOW_SHOWN);
	if(window==NULL){
		fprintf(stderr, "Unable to initialize Window: %s", SDL_GetError());
		return EXIT_FAILURE;
	}
	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(renderer == NULL){
		SDL_DestroyWindow(window);
		fprintf(stderr, "Unable to create the renderer %s", SDL_GetError());
		return EXIT_FAILURE;
	}
	if(TTF_Init() == -1){
		fprintf(stderr, "Failed to initialize TTF %s\n", TTF_GetError());
		return EXIT_FAILURE;
	}
	font= TTF_OpenFont("./Sans.ttf" , FONT_SIZE);
	if (font == NULL) {
		fprintf(stderr, "error: %s\n", TTF_GetError());
		return EXIT_FAILURE;
    }
	SDL_Event e;
	render_game(renderer);
	while(!quit){
		while(SDL_PollEvent(&e) != 0){
			if(e.type == SDL_QUIT)
				quit=1;
			if(e.type == SDL_MOUSEBUTTONDOWN){
				/*
				if(e.button.x<BORDER){
					find_solution();
					render_game(renderer);
				}
				*/
				cell[0] = (e.button.x-BORDER)/CELL_WIDTH; cell[1] = e.button.y/CELL_HEIGHT;
				click_on_cell(renderer, cell[0], cell[1]);
				w=1;
			}
			if(e.type == SDL_KEYDOWN){
				switch(e.key.keysym.sym){
					case SDLK_0:
					case SDLK_KP_0:
					case SDLK_KP_00:
					case SDLK_KP_000:
						if(w){
							w=0;
							b[cell[0]][cell[1]]=0;
						}
						break;
					case SDLK_1:
					case SDLK_KP_1:
						if(w){
							w=0;
							b[cell[0]][cell[1]]=1;
							k[cell[0]][cell[1]]=1;
						}
						break;
					case SDLK_2:
					case SDLK_KP_2:
						if(w){
							w=0;
							b[cell[0]][cell[1]]=2;
							k[cell[0]][cell[1]]=1;
						}
						break;
					case SDLK_3:
					case SDLK_KP_3:
						if(w){
							w=0;
							b[cell[0]][cell[1]]=3;
							k[cell[0]][cell[1]]=1;
						}
						break;
					case SDLK_4:
					case SDLK_KP_4:
						if(w){
							w=0;
							b[cell[0]][cell[1]]=4;
							k[cell[0]][cell[1]]=1;
						}
						break;
					case SDLK_5:
					case SDLK_KP_5:
						if(w){
							w=0;
							b[cell[0]][cell[1]]=5;
							k[cell[0]][cell[1]]=1;
						}
						break;
					case SDLK_6:
					case SDLK_KP_6:
						if(w){
							w=0;
							b[cell[0]][cell[1]]=6;
							k[cell[0]][cell[1]]=1;
						}
						break;
					case SDLK_7:
					case SDLK_KP_7:
						if(w){
							w=0;
							b[cell[0]][cell[1]]=7;
							k[cell[0]][cell[1]]=1;
						}
						break;
					case SDLK_8:
					case SDLK_KP_8:
						if(w){
							w=0;
							b[cell[0]][cell[1]]=8;
							k[cell[0]][cell[1]]=1;
						}
						break;
					case SDLK_9:
					case SDLK_KP_9:
						if(w){
							w=0;
							b[cell[0]][cell[1]]=9;
							k[cell[0]][cell[1]]=1;
						}
						break;
					case SDLK_s:
						find_solution();
						break;
					case SDLK_c:
						for(int x=0; x < N; ++x){
							for(int y=0; y< N; ++y){
								b[x][y]=0;
								k[x][y]=0;
							}	
						}
						break;
					default:
						if(w){
							w=0;
							b[cell[0]][cell[1]]=0;
						}
						break;
				}
				render_game(renderer);
			}
		}
		SDL_Delay(10);
	}
	TTF_CloseFont(font);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}

