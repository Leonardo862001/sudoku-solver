#include<stdio.h>
#include<stdlib.h>
#include "logic.h"
extern int b[N][N];
int find_solution(){
	int *c;
	c=find_unassigned();
	if(!c){
		free(c);
		return 1;
	}
	for(int x=1; x < 10; ++x){
		if(!check(c[0], c[1], x)){
			b[c[0]][c[1]]=x;
			if(find_solution()){
				free(c);
				return 1;
			}
			b[c[0]][c[1]]=0;
		}
	}
	free(c);
	return 0;
}
int* find_unassigned(){
	int *a=(int*)malloc(2*sizeof(int));
	for(int y=0; y<N; ++y){
		for(int x=0; x<N; ++x){
			if(b[x][y]==0){
				a[0]=x;a[1]=y;
				return a;
			}
		}
	}
	return NULL;
}
int check_column(int x, int y, int a){
	y=9;
	while(y-- >0){
		if(b[x][y]==a) return 1;
	}
	return 0;
}
int check_row(int x, int y, int a){
	x=9;
	while(x-- >0){
		if(b[x][y]==a) return 1;
	}
	return 0;
}
int check_square(int x, int y, int a){
	x= x - (x%3);
	y= y - (y%3);
	for(int i=0; i < 3; ++i){
		for(int j=0; j<3; ++j){
			if(b[x+i][y+j]==a) return 1;
		}
	}
	return 0;
}
int check(int x, int y, int a){
	static check_function cf[3]={
		check_row,
		check_column,
		check_square
	};
	if(b[x][y]!=0) return 1;
	int ret=0;
#pragma omp parallel for
	for(int i=0; i < 3; ++i){
		ret+=cf[i](x, y, a);
	}
	return ret;
}
